import React from 'react';
import ReactDOM from 'react-dom';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom';
import 'react-toastify/dist/ReactToastify.css';

import './index.css';
import App from './App';
import PrivateRoute from './PrivateRoute';
import Login from './Login';
import Register from './Register';
import { ToastContainer } from 'react-toastify';

const router = (
  <Router>
    <ToastContainer />
    <Switch>
      <PrivateRoute exact path="/" component={App} />
      <Route path="/login" component={Login} />
      <Route path="/register" component={Register} />
    </Switch>
  </Router>
);

ReactDOM.render(router, document.getElementById('root'));
