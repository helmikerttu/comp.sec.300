import { useState } from 'react';
import { toast } from 'react-toastify';
import Cookies from 'js-cookie';
import { httpRequest, isSuccess } from './helpers';
import './common.css';


export default function Login(props) {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const login = async () => {
    let info = {};
    try {
      info = await httpRequest('http://localhost:8000/auth/login', 'POST', {
        username,
        password
      });
    }
    catch (error) {
      toast.error('Something went wrong!');
      console.log(error);
      return;
    }
    if (isSuccess(info.status)) {
      Cookies.set('Auth token', info.data.token);
      props.history.push('/');
      toast.success('Logged in!');
    }
    else {
      setUsername('');
      setPassword('');
      toast.error(info.data.msg ?? info.data.detail[0].msg ?? 'Something went wrong!');
    }
  }

  const register = () => {
    props.history.push('/register');
  }

  return (
    <div>
      <div className="container header">
        <div className="title">Log in.</div>
      </div>
      <div className="container">
        <div className="container-row">
          <label htmlFor="username" className="label">Username:</label>
          <input
            id="username"
            className="input"
            type="text"
            value={username}
            onChange={(e) => setUsername(e.target.value)}
          />
        </div>
        <div className="container-row">
          <label htmlFor="password" className="label">Password:</label>
          <input
            id="password"
            className="input"
            type="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </div>
        <button className="button green m-50" onClick={login}>Log in.</button>
        <button className="button blue m-50" onClick={register}>Register.</button>
      </div>
    </div>
  )
}