import { useEffect, useState } from 'react';
import { toast } from 'react-toastify';
import { isSuccess, httpRequest } from './helpers';
import './common.css';
import ChangePassword from './ChangePassword';

function Settings(props) {
  const [email, setEmail] = useState('');

  const changeEmail = async () => {
    const result = await httpRequest(
      `http://localhost:8000/user/${props.user.id}`,
      'PATCH',
      { ...props.user, email },
    );
    if (!isSuccess(result.status)) {
      toast.error('Failed to save email.');
    }
    else {
      toast.success('Email saved!');
    }
  };

  useEffect(() => {
    setEmail(props.user.email);
  }, [props.user?.email]);
  
  return (
    <div>
      <div className="subtitle">Settings</div>
      <div className="container-row">
        <div className="label">Role: {props.user.role}</div>
      </div>
      <div className="container-row">
        <label htmlFor="email" className="label">Email:</label>
        <input
          id="email"
          className="input"
          type="text"
          value={props.user.email}
          onChange={(e) => setEmail(e.target.value)}
        />
      </div>
      <div className="container-row">
        <button className="button green m-50" onClick={changeEmail}>Change email.</button>
      </div>
      <ChangePassword user={props.user} />
      <div className="container-row">
        <button className="button red m-50" onClick={props.onCancel}>Back.</button>
      </div>
    </div>
  );
}

export default Settings;