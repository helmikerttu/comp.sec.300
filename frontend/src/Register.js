import { useState } from 'react';
import { toast } from 'react-toastify';
import { httpRequest, isSuccess } from './helpers';
import './common.css';

export default function Register(props) {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [password2, setPassword2] = useState('');

  const register = async () => {
    if (password !== password2) {
      setPassword('');
      setPassword2('');
      toast.error("Passwords don't match!");
      return;
    }
    let info;
    try {
      info = await httpRequest('http://localhost:8000/auth/register', 'POST', {
        username,
        password
      });
    }
    catch (error) {
      toast.error('Something went wrong!');
      console.log(error);
      return;
    }
    if (isSuccess(info.status)) {
      toast.success('Registration success!');
      props.history.push('/login');
    }
    else {
      toast.error(info.data.msg ?? info.data.detail[0].msg ?? 'Something went wrong!');
      setUsername('')
      setPassword('');
      setPassword2('');
    }
  }

  return (
    <div>
      <div className="container header">
        <div className="title">Register.</div>
      </div>
      <div className="container">
        <div className="container-row">
          <label htmlFor="username" className="label">Username:</label>
          <input
            id="username"
            type="text"
            className="input"
            value={username}
            onChange={(e) => setUsername(e.target.value)}
          />
        </div>
        <div className="container-row">
          <label htmlFor="password" className="label">Password:</label>
          <input
            id="password"
            type="password"
            className="input"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
        </div>
        <div className="container-row">
          <label htmlFor="password2" className="label">Repeat password:</label>
          <input
            id="password2"
            type="password"
            className="input"
            value={password2}
            onChange={(e) => setPassword2(e.target.value)}
          />
        </div>
        <button className="button green m-50" onClick={register}>Register.</button>
        <button className="button red m-50" onClick={() => props.history.push('/login')}>Cancel.</button>
      </div>
    </div>
  )
}