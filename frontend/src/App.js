import './App.css';
import { useState } from 'react';
import Cookies from 'js-cookie';
import Settings from './Settings';
import AdminPanel from './AdminPanel';

import './common.css';

const views = {
  ADMIN: 0,
  DASHBOARD: 1,
  SETTINGS: 2,
}

function App(props) {
  const [currentView, setCurrentView] = useState(views.DASHBOARD);

  const logOut = () => {
    Cookies.remove('Auth token');
    props.history.push('/login');
  }

  return (
    <div>
      <div className="container header">
        <div className="title">Secure Programming.</div>
      </div>
      <div className="container content">
        {currentView === views.DASHBOARD && 
          <>
            <div className="subtitle">Hello {props.user.username}!</div>
            <button className="button yellow m-50" onClick={() => setCurrentView(views.SETTINGS)}>Settings.</button>
            {props.user.role === 'ADMIN' && 
              <button className="button blue m-50" onClick={() => setCurrentView(views.ADMIN)}>Admin Panel.</button>
            }
            <button className="button red m-50" onClick={logOut}>Log out.</button>
          </>
        }
        {currentView === views.ADMIN && <AdminPanel onCancel={() => setCurrentView(views.DASHBOARD)} />}
        {currentView === views.SETTINGS && <Settings user={props.user} onCancel={() => setCurrentView(views.DASHBOARD)} />}
      </div>
    </div>
  );
}

export default App;
