import { useState } from 'react';
import { toast } from 'react-toastify';
import { isSuccess, httpRequest } from './helpers';
import './common.css';

function ChangePassword(props) {
  const [currentpw, setCurrentpw] = useState('');
  const [newpw, setNewpw] = useState('');
  const [newpw2, setNewpw2] = useState('');

  const changePassword = async () => {
    if (newpw !== newpw2) {
      setNewpw('');
      setNewpw2('');
      toast.error("Passwords don't match!");
      return;
    }
    const result = await httpRequest(
      `http://localhost:8000/auth/change-password`,
      'POST',
      { old_password: currentpw, new_password: newpw },
    );
    if (!isSuccess(result.status)) {
      toast.error('Failed to change password.');
    }
    else {
      toast.success('Password changed!');
    }
    setCurrentpw('');
    setNewpw('');
    setNewpw2('');
  };

  return (
    <div>
      <div className="container-row">
        <label htmlFor="currentpw" className="label">Current password:</label>
        <input
          id="currentpw"
          className="input"
          type="password"
          value={currentpw}
          onChange={(e) => setCurrentpw(e.target.value)}
        />
      </div>
      <div className="container-row">
      <label htmlFor="newpw" className="label">New password:</label>
      <input
        id="newpw"
        className="input"
        type="password"
        value={newpw}
        onChange={(e) => setNewpw(e.target.value)}
      />
    </div>
    <div className="container-row">
      <label htmlFor="repeatnewpw" className="label">Repeat new password:</label>
      <input
        id="repeatnewpw"
        className="input"
        type="password"
        value={newpw2}
        onChange={(e) => setNewpw2(e.target.value)}
      />
    </div>
    <div className="container-row">
      <button className="button green m-50" onClick={changePassword}>Change password.</button>
    </div>
  </div>
  );
}

export default ChangePassword;