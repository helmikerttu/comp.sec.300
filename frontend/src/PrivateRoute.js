import { useState } from 'react';
import Cookies from 'js-cookie';
import { Route, Redirect } from 'react-router-dom';
import { useAsyncEffect } from 'use-async-effect';
import { httpRequest, isSuccess } from './helpers';

function PrivateRoute({ component: Component, ...rest }) {
  const [user, setUser] = useState();

  // Get current user
  useAsyncEffect(async (isMounted) => {
    const validateInfo = await httpRequest('http://localhost:8000/auth/validate', 'GET');
    if (!isMounted()) return;
    if (!isSuccess(validateInfo.status)) {
      setUser(null);
      return;
    }
    const userInfo = await httpRequest(`http://localhost:8000/user/${validateInfo.data.userId}`, 'GET');
    if (!isMounted()) return;
    if (!isSuccess(userInfo.status)) {
      setUser(null);
      return;
    }
    setUser(userInfo.data.user);
  }, []);

  function render(props) {
    if (Cookies.get('Auth token')) {
      if (user === undefined) {
        return <div>Validating...</div>;
      }
      if (user !== null) {
        return <Component {...props} user={user} setUser={setUser} />;
      }
    }
    return <Redirect to="/login" />;
  }

  return <Route {...rest} render={render} />;
}

export default PrivateRoute;
