import { useState } from 'react';
import useAsyncEffect from 'use-async-effect';
import { toast } from 'react-toastify';
import { httpRequest, isSuccess } from './helpers'; 
import './common.css';

function AdminPanel(props) {
  const [users, setUsers] = useState([]);
  const [modifiedIds, setModifiedIds] = useState(new Set());

  useAsyncEffect(async () => {
    const { data, status } = await httpRequest('http://localhost:8000/user/');
    if (isSuccess(status)) {
      setUsers(data.users);
    }
  }, []);

  const updateUser = (user) => {
    setUsers(users.map(u => u.id === user.id ? user : u));
    setModifiedIds(modifiedIds.add(user.id));
  };

  const saveChanges = async () => {
    const requests = users
      .filter(u => modifiedIds.has(u.id))
      .map(u => httpRequest(`http://localhost:8000/user/${u.id}`, 'PATCH', u));
    const results = await Promise.all(requests);
    const failed = results.filter(res => !isSuccess(res.status));
    if (failed.length > 0) {
      toast.error('Failed to save some data.');
    }
    else {
      toast.success('Changes saved!');
    }
    setModifiedIds(new Set());
  };

  return (
    <div>
      <div className="subtitle">Admin panel</div>
      <table className="table">
        <tr  className="tr">
          <td className="th">Id</td>
          <td className="th">Username</td>
          <td className="th">Role</td>
          <td className="th">Email</td>
        </tr>
        {users && users.map(u => (
          <tr key={u.id} className="tr">
            <td className="td">{u.id}</td>
            <td className="td">
              <input
                value={u.username || ''}
                onChange={e => updateUser({ ...u, username: e.target.value })}
              />
            </td>
            <td className="td">
              <select
                value={u.role}
                onChange={e => updateUser({ ...u, role: e.target.value })}
              >
                <option value="ADMIN">ADMIN</option>
                <option value="USER">USER</option>
              </select>
            </td>
            <td className="td">
              <input
                value={u.email || ''}
                onChange={e => updateUser({ ...u, email: e.target.value })}
              />
            </td>
          </tr>
        ))}
      </table>
      <div className="container-row">
        <button className="button green m-50" onClick={saveChanges}>Save changes.</button>
      </div>
      <div className="container-row">
        <button className="button red m-50" onClick={props.onCancel}>Back.</button>
      </div>
    </div>
  );
}

export default AdminPanel;