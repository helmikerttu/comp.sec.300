# COMP.SEC.300 Project
*Markus Meskanen & Vilma Salmikuukka*

## About

This project implements a minimalistic web-application with a basic user system.
The frontend is implemented with JavaScript+React and the backend is implemented with Python+FastAPI.

Key functional features include:
- Login/Register
- Password change
- User properties
    - `email` implemented as an example, adding more is trivial
- User roles
    - Currently `USER` and `ADMIN`, adding more is trivial
- Admin panel for user management
    - List users, change usernames, change emails, change user roles

## Installation and running

Docker is the recommended method.

### With Docker

1. Install Docker and Docker Compose (if not included with Docker). https://docs.docker.com/compose/
2. Run `docker-compose up` in the project root.
3. Go to `http://localhost:3000/` on your browser.

You can register a new user, or you can login as admin using `admin`/`admin123` credentials.
Admin's default password and other environment variables can be edited in `docker-compose.yml`.

### Without Docker (e.g. manually)

Both `backend` and `frontend` directories have distinct `README.md` files that explain how to install and run the application in question.
It'll be much more steps than just using Docker, but you'll understand better how the application works.
You obviously need to have both front- and backend running to operate the application.

## Notable security features

Most security features were implemented as manually as reasonable, but obviously certain features were delegated to third-party packages.
While these packages were investigated as thoroughly as reasonable, use them and this application at your own risk.
We did not read the entire source code of every single package used.

Here's a table of the most notable security features that were implemented:

Feature            | Implementation
-------------------|----------------
Password hash&salt | `bcrypt` Python package.
Password length    | `PASSWORD_MIN_LEN` environment variable. Use it: https://xkcd.com/936/.
JWT algorithms     | `PyJWT` Python package with "HS256". Expires in 1 hour.
JWT secret         | Load from environment variable.
CORS               | FastAPI built-in feature. Only allow http://localhost:3000.
HTTPS              | Uvicorn built-in feature: https://www.uvicorn.org/deployment/#running-with-https.
Data sanitization  | SQLAlchemy built-in feature.
