# COMP.SEC.300 Backend

You need to have Python 3 installed before running the server.

## Quick start

1. Create a virtual environment: `py -3 -m venv myenv`
2. Activate the virtual environment:
    - Linux: `source myenv/Scripts/activate`
    - Windows: `./myenv/Scripts/activate.bat`
3. Install requirements: `pip install -r requirements.txt`
    - You might need Visual Studio C++ build tools: https://visualstudio.microsoft.com/visual-cpp-build-tools/
    - You might need newer version of pip: `python -m pip install --upgrade pip`
4. Create the database (it's a local SQLite file): `python -m app --create-tables`
5. Set a secret environment variable for JWT tokens: `export COMP_SEC_SECRET=<your-secret>`
    - You can generate a random secret with `openssl rand -hex 32`
6. Set `DATABASE_URI`, `PASSWORD_MIN_LEN`, and `ADMIN_PASSWORD` environment variables.
    - You can use `export DATABASE_URI=sqlite://` to use SQLite's `:memory:` database for quick testing.
    - More about SQLAlchemy's database URI: https://docs.sqlalchemy.org/en/14/core/engines.html
7. Run FastAPI server with `uvicorn app:app`
    - Use `--reload` for development
    - Use HTTPS for production: https://www.uvicorn.org/deployment/#running-with-https

The server should now be running at `http://localhost:8000/`.

You can also use `dev.sh` script to easily re-run steps 2, 5, 6, and 7 for development.
