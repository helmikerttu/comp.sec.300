from datetime import datetime, timedelta
import os

import bcrypt
import jwt

from .env import SECRET_KEY
from .models import User


def hash_password(password):
    salt = bcrypt.gensalt()
    return bcrypt.hashpw(password.encode('utf-8'), salt).decode('utf-8')


def check_password(password, hashed):
    return bcrypt.checkpw(password.encode(), hashed.encode())


def sign_jwt(userId, exp=1):
    return jwt.encode(
        {
            'userId': userId,
            'exp': datetime.utcnow() + timedelta(hours=exp),
        },
        SECRET_KEY,
        algorithm='HS256',
    )


def parse_jwt(token):
    return jwt.decode(token, SECRET_KEY, algorithms=['HS256']).get('userId')


def get_user(token, session):
    try:
        userId = parse_jwt(token)
    except (jwt.ExpiredSignatureError, jwt.InvalidSignatureError):
        return None
    if userId is None:
        return None
    return session.query(User).filter_by(id=userId).one_or_none()
