from enum import Enum

from sqlalchemy import Column, Enum, Integer, String

from .database import Base


class UserRole(str, Enum):
    USER = 'USER'
    ADMIN = 'ADMIN'


class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True, index=True)
    username = Column(String, unique=True, index=True)
    password = Column(String)
    email = Column(String, nullable=True)
    role = Column(String, default=UserRole.USER)

    def public_json(self):
        return {
            'id': self.id,
            'username': self.username,
            'email': self.email,
            'role': self.role,
        }
        