from fastapi import APIRouter, Depends, Response, status
from fastapi.security import OAuth2PasswordBearer
from pydantic import BaseModel, Field
from sqlalchemy.exc import IntegrityError

from .. import env
from ..models import User
from ..database import Session
from ..operations import *

router = APIRouter(prefix='/auth')
oauth2 = OAuth2PasswordBearer(tokenUrl="token")


class AuthInfo(BaseModel):
    username: str = Field(None, min_length=4, max_length=24)
    password: str = Field(None, min_length=env.PASSWORD_MIN_LEN)


class PasswordChangeInfo(BaseModel):
    old_password: str = Field(None, min_length=env.PASSWORD_MIN_LEN)
    new_password: str = Field(None, min_length=env.PASSWORD_MIN_LEN)


@router.post('/register')
async def register(info: AuthInfo, response: Response):
    with Session() as db:
        user = User(username=info.username, password=hash_password(info.password))
        db.add(user)
        try:
            db.commit()
        except IntegrityError:
            response.status_code = status.HTTP_400_BAD_REQUEST
            return {'msg': 'Username taken'}

        response.status_code = status.HTTP_201_CREATED
        return {
            'msg': 'Register success',
            'userId': user.id,
        }


@router.post('/login')
async def login(info: AuthInfo, response: Response):
    with Session() as db:
        user = db.query(User).filter_by(username=info.username).one_or_none()
    if user is None or not check_password(info.password, user.password):
        response.status_code = status.HTTP_401_UNAUTHORIZED
        return {'msg': 'Invalid username or password'}

    return {
        'msg': 'Login success',
        'token': sign_jwt(user.id),
    }


@router.get('/validate')
async def validate(response: Response, token: str=Depends(oauth2)):
    with Session() as db:
        user = get_user(token, db)

    if user is None:
        response.status_code = status.HTTP_401_UNAUTHORIZED
        return {'msg': 'Invalid token'}

    return {
        'msg': 'Logged in',
        'userId': user.id,
    }


@router.post('/change-password')
async def change_password(info: PasswordChangeInfo, response: Response, token: str=Depends(oauth2)):
    with Session() as db:
        user = get_user(token, db)
        if user is None:
            response.status_code = status.HTTP_401_UNAUTHORIZED
            return {'msg': 'Invalid token'}

        if not check_password(info.old_password, user.password):
            response.status_code = status.HTTP_401_UNAUTHORIZED
            return {'msg': 'Incorrect password'}
 
        user.password = hash_password(info.new_password)
        db.commit()
        return {'msg': 'Password changed successfully'}
