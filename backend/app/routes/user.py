from typing import Optional

from fastapi import APIRouter, Depends, Response, status
from fastapi.security import OAuth2PasswordBearer
from pydantic import BaseModel, Field

from ..models import User, UserRole
from ..database import Session
from ..operations import get_user


router = APIRouter(prefix='/user')
oauth2 = OAuth2PasswordBearer(tokenUrl="token")


class UpdateUserInfo(BaseModel):
    username: Optional[str] = Field(None, min_length=4, max_length=24)
    role: Optional[UserRole] = None
    email: Optional[str] = Field(None)


@router.get('/')
async def get_users():
    with Session() as db:
        users = db.query(User).all()
    return {'users': [user.public_json() for user in users]}


@router.get('/{userId}')
async def get_user_by_id(userId: int, response: Response):
    with Session() as db:
        user = db.query(User).filter_by(id=userId).one_or_none()
    if user is None:
        response.status_code = status.HTTP_204_NO_CONTENT
        return
    
    return {'user': user.public_json()}


@router.patch('/{userId}')
async def update_user(userId: int, info: UpdateUserInfo, response: Response, token: str=Depends(oauth2)):
    with Session() as db:
        user = get_user(token, db)

        if user is None or (user.id != userId and user.role != UserRole.ADMIN):
            response.status_code = status.HTTP_401_UNAUTHORIZED
            return {'msg': 'Unauthorized operation'}

        if user.role != UserRole.ADMIN and info.role == UserRole.ADMIN:
            response.status_code = status.HTTP_401_UNAUTHORIZED
            return {'msg': 'Only admin can set ADMIN role'}

        target = db.query(User).filter_by(id=userId).one_or_none()
        if not target:
            return {'msg': 'Invalid userId'}

        if info.username is not None:
            target.username = info.username
        if info.role is not None:
            target.role = info.role
        if info.email is not None:
            target.email = info.email
        db.commit()

    return {'msg': 'Update success'}
