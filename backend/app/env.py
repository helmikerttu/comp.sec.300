import os

def get_env_var(name):
    value = os.getenv(name)
    if value is None:
        raise EnvironmentError(f'Missing {name} environment variable')
    return value


SECRET_KEY = get_env_var('COMP_SEC_SECRET')

DATABASE_URI = get_env_var('DATABASE_URI')

PASSWORD_MIN_LEN = get_env_var('PASSWORD_MIN_LEN')
try:
    PASSWORD_MIN_LEN = int(PASSWORD_MIN_LEN)
except ValueError:
    raise EnvironmentError('PASSWORD_MIN_LEN environment variable is not an integer')
if PASSWORD_MIN_LEN < 8:
    raise EnvironmentError('PASSWORD_MIN_LEN must be 8 or more (please more)')

ADMIN_PASSWORD = get_env_var('ADMIN_PASSWORD')
if len(ADMIN_PASSWORD) < PASSWORD_MIN_LEN:
    raise EnvironmentError(f'ADMIN_PASSWORD must be {PASSWORD_MIN_LEN} characters or more')
