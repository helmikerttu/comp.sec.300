from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from sqlalchemy.exc import IntegrityError

from .routes import auth
from .routes import user
from .operations import hash_password
from . import env
from . import database
from . import models  # Database tables

app = FastAPI()
app.include_router(auth.router)
app.include_router(user.router)

app.add_middleware(
    CORSMiddleware,
    allow_origins=['http://localhost:3000', 'http://localhost:8000'],
    allow_credentials=True,
    allow_methods=['*'],
    allow_headers=['*'],
)

# Initialize database tables
database.Base.metadata.create_all(database.engine)

# Initialize 'admin' user
with database.Session() as db:
    admin = db.query(models.User).filter_by(username='admin').one_or_none()

    if admin is None:
        admin = models.User(
            username='admin',
            password=hash_password(env.ADMIN_PASSWORD),
            role=models.UserRole.ADMIN,
        )
        db.add(admin)
        try:
            db.commit()
            print('Admin initialized')
        except IntegrityError:
            print('Unable to initialize admin user!')
