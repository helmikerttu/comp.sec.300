import argparse
from . import database
from .models import User, UserRole

parser = argparse.ArgumentParser()
parser.add_argument('--reset-tables', dest='reset_tables', action='store_true')
parser.add_argument('--set-admin', dest='admin', default=None)
args = parser.parse_args()

if args.reset_tables:
    database.Base.metadata.drop_all(database.engine)
    database.Base.metadata.create_all(database.engine)
    print('Tables re-created successfully')

if args.admin is not None:
    with database.Session() as db:
        user = db.query(User).filter_by(username=args.admin).one_or_none()
        if user is None:
            print(f'Invalid user {args.admin}')
        else:
            user.role = UserRole.ADMIN
            db.commit()
            print(f'{args.admin} is now an ADMIN')