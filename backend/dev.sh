#!/bin/bash
source env/Scripts/activate
export COMP_SEC_SECRET=kissa
export ADMIN_PASSWORD=admin123
export DATABASE_URI=sqlite://
export PASSWORD_MIN_LEN=4
uvicorn app:app --reload
